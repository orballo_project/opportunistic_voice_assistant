import torch
from torchaudio.models.wav2vec2.utils.import_huggingface import import_huggingface_model
from transformers import Wav2Vec2ForCTC

audio_len = 80000 # 5s 16khz

# Load Wav2Vec2 pretrained model from Hugging Face Hub
model = Wav2Vec2ForCTC.from_pretrained("ifrz/gl_wav2vec2_base")

# Convert the model to torchaudio format
model = import_huggingface_model(model)

model = model.eval()

input = torch.zeros(1, audio_len)

torch.onnx.export(model, input, "wav2vec2-base-gl.onnx",
                  input_names=["input"],
                  output_names=["output"],
                  do_constant_folding=True,       # whether to execute constant folding for optimization
                  dynamic_axes={'input' : {1 : 'audio_len'},    # variable length axes
                                'output': {1 : 'audio_len'}},
                  opset_version=13)

# avoid dynamic quantization of convolutional nodes on wav2vec2-base models, for cnn onnx recommends static
n_list = [
    'Conv_2',
    'Conv_20',  # more than 400ms 
    'Conv_29',  # more than 200ms
    'Conv_38',
    'Conv_47',
    'Conv_56',
    'Conv_65',
    'Conv_89'
    ]

def quantize_onnx_model(onnx_model_path, quantized_model_path):
    print("Starting quantization...")
    from onnxruntime.quantization import quantize_dynamic, QuantType
    quantize_dynamic(onnx_model_path,
                     quantized_model_path,
                     weight_type=QuantType.QUInt8,
                     nodes_to_exclude=n_list)
    
    print(f"Quantized model saved to: {quantized_model_path}")

quantized_model_name = "wav2vec2-base-gl.qint8.onnx"
quantize_onnx_model('wav2vec2-base-gl.onnx', quantized_model_name)
