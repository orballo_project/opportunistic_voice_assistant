# Android Speech Recognition Example

IMPORTANT 

This is a modified version of the official example adapted to work with wav2vec2, in order to test this example is neccesary to generate a valid wav2vec2 model (see the scripts in the parent folder) an place it in res/raw folder, also check the label list for the desired language.

Official android example with Whisper [here](https://github.com/microsoft/onnxruntime-inference-examples/tree/main/mobile/examples/whisper/local/android)
