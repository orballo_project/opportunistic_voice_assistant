package ai.onnxruntime.example.speechrecognition

import ai.onnxruntime.OnnxTensor
import ai.onnxruntime.OrtEnvironment
import ai.onnxruntime.OrtSession
import ai.onnxruntime.extensions.OrtxPackage
import android.os.SystemClock
import android.util.Log


class SpeechRecognizer(modelBytes: ByteArray) : AutoCloseable {
    private val session: OrtSession
//    private val baseInputs: Map<String, OnnxTensor>

    private val labels = arrayOf("", "<s>", "</s>", "⁇", " ", "'", "-", "a", "b", "c", "d", "e", "f", "g",
        "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
        "z", "á", "é", "í", "ñ", "ó", "ö", "ú", "ü")

    init {
        val env = OrtEnvironment.getEnvironment()
        val sessionOptions = OrtSession.SessionOptions()
        sessionOptions.registerCustomOpLibrary(OrtxPackage.getLibraryPath())
        Log.d(MainActivity.TAG, "DEBUG ort options $sessionOptions")

        session = env.createSession(modelBytes, sessionOptions)
        /*
                val nMels: Long = 80
                val nFrames: Long = 3000

                baseInputs = mapOf(
                    "min_length" to createIntTensor(env, intArrayOf(1), tensorShape(1)),
                    "max_length" to createIntTensor(env, intArrayOf(200), tensorShape(1)),
                    "num_beams" to createIntTensor(env, intArrayOf(1), tensorShape(1)),
                    "num_return_sequences" to createIntTensor(env, intArrayOf(1), tensorShape(1)),
                    "length_penalty" to createFloatTensor(env, floatArrayOf(1.0f), tensorShape(1)),
                    "repetition_penalty" to createFloatTensor(env, floatArrayOf(1.0f), tensorShape(1)),
                    "attention_mask" to createIntTensor(
                        env, IntArray((1 * nMels * nFrames).toInt()) { 0 },
                        tensorShape(1, nMels, nFrames)
                    ),
                )*/
    }

    data class Result(val text: String, val inferenceTimeInMs: Long)


    fun run(audioTensor: OnnxTensor): Result {
        /// WHISPER
        // model output (41 is the number of labels)
        // [1, num_frames, 41]

        val inputs = mutableMapOf<String, OnnxTensor>()
        var hypothesis = ""
        //  baseInputs.toMap(inputs)
        //  inputs["audio_pcm"] = audioTensor
        inputs["input"] = audioTensor
        val startTimeInMs = SystemClock.elapsedRealtime()
        val outputs = session.run(inputs)
        val elapsedTimeInMs = SystemClock.elapsedRealtime() - startTimeInMs
        val recognizedText = outputs.use {
            @Suppress("UNCHECKED_CAST")
            val rawOutput = (outputs[0].value as Array<Array<FloatArray>>)
            //Log.d(MainActivity.TAG, "DEBUG number of frames: ${rawOutput[0].size}")
            val bestPath = IntArray(rawOutput[0].size)
            for (i in 0 until rawOutput[0].size) {
                // index of the highest value (aka label) of each frame - similar to torch.argmax
                val maxIdx = rawOutput[0][i].indices.maxBy { rawOutput[0][i][it] } ?: -1
                //Log.d(MainActivity.TAG, "DEBUG max score label for frame $i -- ${labels[maxIdx]}")
                bestPath[i] = maxIdx
            }
            var prev = ""
            for (i in bestPath) {
                var char = labels[i]
                if (char == prev)
                    continue
                if (char == "<s>") {
                    prev = ""
                    continue
                }
                hypothesis += char
                prev = char
            }
            hypothesis.replace('|', ' ')
        }
        return Result(recognizedText, elapsedTimeInMs)
    }

    override fun close() {/*
        baseInputs.values.forEach {
            it.close()
        }*/
        session.close()
    }
}