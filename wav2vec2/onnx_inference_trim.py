import soundfile as sf
import torch
from transformers import Wav2Vec2Processor
import onnxruntime as rt
import numpy as np
import time
import librosa

# Improvements: 
# - gpu / cpu flag
# - convert non 16 khz sample rates
# - inference time log

class Wave2Vec2ONNXInference():
    def __init__(self,model_name,onnx_path):
        self.processor = Wav2Vec2Processor.from_pretrained(model_name) 
        #self.model = Wav2Vec2ForCTC.from_pretrained(model_name)
        options = rt.SessionOptions()
        options.graph_optimization_level = rt.GraphOptimizationLevel.ORT_ENABLE_ALL
        options.intra_op_num_threads = 4
#        options.inter_op_num_threads = 2 # use with parallel
#        options.enable_profiling = True
#        options.execution_mode = rt.ExecutionMode.ORT_PARALLEL
        self.model = rt.InferenceSession(onnx_path, options)

    def buffer_to_text(self,audio_buffer):
        if(len(audio_buffer)==0):
            return ""

#        wavtrim = torchaudio.functional.vad(torch.tensor(audio_buffer), 16_000, trigger_level=12)
        yt, index = librosa.effects.trim(audio_buffer, top_db=24)#32)
        inputs = self.processor(torch.tensor(yt), sampling_rate=16_000, return_tensors="np", padding=True)

        input_values = inputs.input_values
        onnx_outputs = self.model.run(None, {self.model.get_inputs()[0].name: input_values})[0]
        prediction = np.argmax(onnx_outputs, axis=-1)

        transcription = self.processor.decode(prediction.squeeze().tolist())
        return transcription.lower()

    def file_to_text(self,filename):
        audio_input, samplerate = sf.read(filename)
        assert samplerate == 16000
        return self.buffer_to_text(audio_input)

if __name__ == "__main__":
    print("Model test")
    mst = time.time()
    asr = Wave2Vec2ONNXInference("ifrz/gl_wav2vec2_base","wav2vec2-base-gl.qint8.onnx")
    men = time.time()

    st = time.time()
    text = asr.file_to_text("../test.wav")
    en = time.time()
    print(text)
    print('Time transcription:', en - st)

    print('Time load model:', men - mst)
