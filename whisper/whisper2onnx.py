from optimum.onnxruntime import ORTModelForSpeechSeq2Seq
from transformers import (
    set_seed,
    AutoProcessor
)
from pathlib import Path
import os

SEED = 42

# Export vanilla & optimized onnx model
def export_vanilla_optimized_onnx(model_checkpoint):
    set_seed(SEED)
    processor = AutoProcessor.from_pretrained(model_checkpoint)

    # Vanilla
    model = ORTModelForSpeechSeq2Seq.from_pretrained(model_checkpoint, from_transformers=True, use_cache=True)
    onnx_path = Path(os.path.join("onnx_models/", model_checkpoint))
    model.save_pretrained(onnx_path)
    processor.save_pretrained(onnx_path)

# Export dynamic qint8 & optimized onnx model
# done in inference_whisper_transformers.py

export_vanilla_optimized_onnx('ifrz/whisper-gl-tiny')

