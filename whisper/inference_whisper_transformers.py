from datasets import load_dataset
from transformers import AutoProcessor, pipeline
from optimum.onnxruntime import ORTModelForSpeechSeq2Seq
import librosa

audio, rate = librosa.load("../test.wav", sr = 16000)

processor = AutoProcessor.from_pretrained("ifrz/whisper-gl-tiny")
model = ORTModelForSpeechSeq2Seq.from_pretrained("ifrz/whisper-gl-tiny", from_transformers=True)
speech_recognition_pipeline = pipeline(
 "automatic-speech-recognition",
  model=model,
  feature_extractor=processor.feature_extractor,
  tokenizer=processor.tokenizer,
 )

import time
startTime = time.time()
result = speech_recognition_pipeline(audio)
executionTime = (time.time() - startTime)

print("result:", result)
#print('Execution time model in seconds: ' + str(executionTime)) ### use session profile for times

#####

from transformers import AutoTokenizer, AutoConfig

model_checkpoint = "ifrz/whisper-gl-tiny"
save_directory = "./onnx/"
# Load a model from transformers and export it to ONNX
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)
ort_model = ORTModelForSpeechSeq2Seq.from_pretrained(model_checkpoint, from_transformers=True)
# Save the ONNX model and tokenizer
ort_model.save_pretrained(save_directory)
tokenizer.save_pretrained(save_directory)

from optimum.onnxruntime.configuration import AutoQuantizationConfig
from optimum.onnxruntime import ORTQuantizer

##### cannot quantize multiple files, create different paths copy onnx models and config
##### also cannot convert onnx model on arm device, convert it on a PC and test infer on arm64

# Define the quantization methododecoder_with_pastlogy
qconfig = AutoQuantizationConfig.arm64(is_static=False, per_channel=False)
quantizer = ORTQuantizer.from_pretrained('./onnx/decoder_with_past')
quantizer.quantize(save_dir=save_directory, quantization_config=qconfig)
quantizer = ORTQuantizer.from_pretrained('./onnx/decoder')
quantizer.quantize(save_dir=save_directory, quantization_config=qconfig)
quantizer = ORTQuantizer.from_pretrained('./onnx/encoder')
quantizer.quantize(save_dir=save_directory, quantization_config=qconfig)




options = ort.SessionOptions()
options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
options.intra_op_num_threads = 4
#options.inter_op_num_threads = 2 # use with parallel
#options.enable_profiling = True
#options.execution_mode = rt.ExecutionMode.ORT_PARALLEL


config = AutoConfig.from_pretrained("ifrz/whisper-gl-tiny")    
model = ORTModelForSpeechSeq2Seq.from_pretrained(
        model_id="./onnx",
        encoder_file_name="./encoder_model_quantized.onnx",
        decoder_file_name="./decoder_model_quantized.onnx",
        decoder_with_past_file_name="./decoder_with_past_model_quantized.onnx",
        config=config,
        session_options = options,
    ).to('cpu')
tokenizer = AutoTokenizer.from_pretrained("ifrz/whisper-gl-tiny")

speech_recognition_pipeline = pipeline(
    "automatic-speech-recognition",
     model=model,
     feature_extractor=processor.feature_extractor,
     tokenizer=processor.tokenizer,
  )


startTime = time.time()
result = speech_recognition_pipeline(audio)
executionTime = (time.time() - startTime)

print("result:", result)
#print('Execution time qint8 model in seconds: ' + str(executionTime))
