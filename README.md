# Opportunistic Voice Assistant

The code of this repository has been used for performing the experiments of the following paper: "Design, Implementation, and Practical Evaluation of a Voice Recognition Based IoT Home Automation System for Low-Resource Languages and Resource-Constrained Edge IoT Devices: A System for Galician and Mobile Opportunistic Scenarios". It is possible to find further information on the experiments and on the proposed opportunistic architectures in such paper.

## Introduction

The code provided in this project allows for creating different models for ASR inference on resource constrained devices, each of the models mentioned below were finetunig for Galician.

Huggingface's repository where they are hosted.

- [Repository](https://huggingface.co/ifrz)

wav2vec2 based:

- gl_wav2vec2_distilled: distilled version of the facebook/wav2vec2-large-xlsr-53 and finetuned for Galician language (20 hrs approx.)   
- gl_wav2vec2_base: finetuned facebook/wav2vec2-base-100k-voxpopuli model with Galician language (20 hrs approx.)
- gl_wav2vec2-large-xlsr-53: finetuned facebook/wav2vec2-large-xlsr-53 model with Galician language (20 hrs approx.) 

whisper based:

- whisper-gl-tiny: finetuned openai/whisper-tiny model with Galician language (20 hrs approx.)
- whisper-gl-large: finetuned openai/whisper-large model with Galician language (20 hrs approx. plus augmented audio)

Note: gl_wav2vec2-large-xlsr-53 and whisper-gl-large offers the best results but need resource-intensive devices to perform (especially the whisper-based)

## Requeriments

This repository assumes that the ASR models are already trained, the objective is to generate models optimized for mobile platforms.

## Usage

With the code in this repo it is possible to use these models for inference on either Android or SBCs like the Raspbery Pi, in both cases the inference is performed only on the CPU through the onnxruntime stack.

The ```distilhubert``` folder has the configuration files to train a distilled model of the Wav2Vec2-XLSR-53 pretrain with distilhubert through the [s3prl](https://github.com/s3prl/s3prl/blob/main/s3prl/upstream/distiller/README.md) training stack.

The ```wav2vec2``` folder has the scripts to convert the wav2vec2 models for use on a mobile platform as well as to run the inference on an SBC through python. Inside this directory the ```android-wav2vec2``` folder has a sample project for Android use.

The ```whisper``` folder contains the scripts needed to convert the model to mobile platform and to perform the inference. The Android code can be used with the [official](https://github.com/microsoft/onnxruntime-inference-examples/tree/main/mobile/examples/whisper/local/android) version provided by microsoft.

## Funding

The development presented in this repository have been funded by Opportunistic Edge Computing Based on Mobile and Low-Power IoT Devices (ORBALLO), Grant PID2020-118857RA-I00, funded by MCIN and AEI (10.13039/501100011033).

If you use the code of this repository, please cite us as follows:
```
@ARTICLE{10151879,
  author={Froiz-Míguez, Iván and Fraga-Lamas, Paula and Fernández-CaraméS, Tiago M.},
  journal={IEEE Access}, 
  title={[Design, Implementation, and Practical Evaluation of a Voice Recognition Based IoT Home Automation System for Low-Resource Languages and Resource-Constrained Edge IoT Devices: A System for Galician and Mobile Opportunistic Scenarios}}, 
  year={2023},
  volume={11},
  number={},
  pages={63623-63649},
  doi={10.1109/ACCESS.2023.3286391}
}
```

## License
Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

